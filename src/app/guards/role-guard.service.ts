import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as _ from 'lodash';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const userRole = this._authService.checkRoleId();

    if (_.includes(next.data.role, userRole)) {
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/']);
    return false;
  }
}
