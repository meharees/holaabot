import {Injectable} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class IntercepterService implements HttpInterceptor {

    constructor() {
    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      var httpsReq ;
      httpsReq = request.clone({
        setHeaders: {
          'Content-Type': 'application/json'
        }
      });
        return next.handle(httpsReq);
    }
}
