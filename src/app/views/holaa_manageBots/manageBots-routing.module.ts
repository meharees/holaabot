import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicInfoComponent } from './basic-info/basic-info.component';
import { ChannelsComponent } from './channels/channels.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { KnowledgeComponent } from './knowledge/knowledge.component';
import { ThemeComponent } from './theme/theme.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: BasicInfoComponent,
    data: {
      title: 'basic-info'
    }
  },
  {
    path: 'basic-info',
    component: BasicInfoComponent,
    data: {
      title: 'basic-info'
    }
  },
  {
    path: 'channels',
    component: ChannelsComponent,
    data: {
      title: 'channels'
    }
  },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    data: {
      title: 'configuration'
    }
  },
  {
    path: 'knowledge',
    component: KnowledgeComponent,
    data: {
      title: 'knowledge'
    }
  },
  {
    path: 'theme',
    component: ThemeComponent,
    data: {
      title: 'theme'
    }
  },
  {
    path: 'confirmation',
    component: ConfirmationComponent,
    data: {
      title: 'confirmation'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageBotsRoutingModule {}
