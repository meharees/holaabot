import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ManageBotsRoutingModule } from './manageBots-routing.module';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { ChannelsComponent } from './channels/channels.component';
import { ThemeComponent } from './theme/theme.component';
import { KnowledgeComponent } from './knowledge/knowledge.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ConfirmationComponent } from './confirmation/confirmation.component';

@NgModule({
  imports: [
    FormsModule,
    ManageBotsRoutingModule,
    TabsModule,
    CollapseModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [ BasicInfoComponent, ChannelsComponent, ThemeComponent, KnowledgeComponent, ConfigurationComponent, ConfirmationComponent ]
})
export class ManageBotsModule { }
