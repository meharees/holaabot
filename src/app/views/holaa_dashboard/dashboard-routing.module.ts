import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { ConversationComponent } from './conversation/conversation.component';
import { LeadsComponent } from './leads/leads.component';
import { IntegrationComponent } from './integration/integration.component';
import { ChannelsComponent } from './channels/channels.component';
import { PipelineComponent } from './pipeline/pipeline.component';
import { SettingsComponent } from './settings/settings.component';
import { StatusComponent } from './status/status.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PricingComponent } from './pricing/pricing.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Dashboard'
    },
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'conversation',
        component: ConversationComponent,
        data: {
          title: 'Conversation'
        }
      },
      {
        path: 'leads',
        component: LeadsComponent,
        data: {
          title: 'Leads'
        }
      },
      {
        path: 'integration',
        component: IntegrationComponent,
        data: {
          title: 'Integration'
        }
      },
      {
        path: 'channels',
        component: ChannelsComponent,
        data: {
          title: 'Channels'
        }
      },
      {
        path: 'pipeline',
        component: PipelineComponent,
        data: {
          title: 'Pipeline'
        }
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: {
          title: 'Settings'
        }
      },
      {
        path: 'status',
        component: StatusComponent,
        data: {
          title: 'Status'
        }
      },
      {
        path: 'welcome',
        component: WelcomeComponent,
        data: {
          title: 'Welcome'
        }
      },
      {
        path: 'pricing',
        component: PricingComponent,
        data: {
          title: 'Pricing'
        }
      },
    ]
  },


];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
