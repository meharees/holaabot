import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import {DashboardComponent} from './dashboard.component';
import {ConversationComponent} from './conversation/conversation.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import { LeadsComponent } from './leads/leads.component';
import { IntegrationComponent } from './integration/integration.component';
import { ChannelsComponent } from './channels/channels.component';
import { PipelineComponent } from './pipeline/pipeline.component';
import { SettingsComponent } from './settings/settings.component';
import { StatusComponent } from './status/status.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PricingComponent } from './pricing/pricing.component';

@NgModule({
  imports: [
    FormsModule,
    DashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    CollapseModule.forRoot()
  ],
  declarations: [DashboardComponent, ConversationComponent, LeadsComponent, IntegrationComponent, ChannelsComponent, PipelineComponent, SettingsComponent, StatusComponent, WelcomeComponent, PricingComponent]
})
export class DashboardModule {
}
