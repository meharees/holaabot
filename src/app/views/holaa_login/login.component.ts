import {Component, OnInit, ElementRef} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormArray, FormBuilder, FormControl, Validators} from '@angular/forms';
import {LoginService} from '../../helper/login/login.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['../holaa_signup/signup.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private LS: LoginService) {

  }

  loading = false;
  submitted = false;
  loginForm: FormGroup

  ngOnInit() {

    this.createForm();
  }

  createForm() {
    console.log('create form')
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get form() {
    return this.loginForm.controls;
  }

  loginSubmit() {
    console.log("submit",this.loginForm.value)
    let _this = this;
    this.submitted = true;
    if (this.loginForm.status == 'VALID') {
      let params = {}
      _this.LS.login(params).subscribe(response => {
        console.log("response aaaaa", response);
        _this.router.navigate(['/manage-bots']);
      });
    }


  }
}
