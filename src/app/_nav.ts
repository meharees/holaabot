import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard/',
    icon: 'icon-speedometer',
  },
  {
    name: 'Leads',
    url: '/dashboard/leads',
    icon: 'icon-layers',
  },
  {
    name: 'Conversation',
    url: '/dashboard/conversation',
    icon: 'icon-speech',
  },
  {
    name: 'Integration',
    url: '/dashboard/integration',
    icon: 'icon-link',
  },
  {
    name: 'Channels',
    url: '/dashboard/channels',
    icon: 'icon-graph',
  },
  {
    name: 'Pipeline',
    url: '/dashboard/pipeline',
    icon: 'icon-shield',
  },
  {
    name: 'Settings',
    url: '/dashboard/settings',
    icon: 'icon-settings',
  },
  {
    name: 'Status',
    url: '/dashboard/status',
    icon: 'icon-chart',
  },
  {
    name: 'Welcome',
    url: '/dashboard/welcome',
    icon: 'icon-chart',
  },
  {
    name: 'Pricing',
    url: '/dashboard/pricing',
    icon: 'icon-chart',
  },

  {
    name: 'Dashboard',
    url: '/dashboard/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Theme'
  },
  {
    name: 'Colors',
    url: '/dashboard/theme/colors',
    icon: 'icon-drop'
  },
  {
    name: 'Typography',
    url: '/dashboard/theme/typography',
    icon: 'icon-pencil'
  },
  {
    title: true,
    name: 'Components'
  },
  {
    name: 'Base',
    url: '/dashboard/base',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Cards',
        url: '/dashboard/base/cards',
        icon: 'icon-puzzle'
      },
      {
        name: 'Carousels',
        url: '/dashboard/base/carousels',
        icon: 'icon-puzzle'
      },
      {
        name: 'Collapses',
        url: '/dashboard/base/collapses',
        icon: 'icon-puzzle'
      },
      {
        name: 'Forms',
        url: '/dashboard/base/forms',
        icon: 'icon-puzzle'
      },
      {
        name: 'Navbars',
        url: '/dashboard/base/navbars',
        icon: 'icon-puzzle'

      },
      {
        name: 'Pagination',
        url: '/dashboard/base/paginations',
        icon: 'icon-puzzle'
      },
      {
        name: 'Popovers',
        url: '/dashboard/base/popovers',
        icon: 'icon-puzzle'
      },
      {
        name: 'Progress',
        url: '/dashboard/base/progress',
        icon: 'icon-puzzle'
      },
      {
        name: 'Switches',
        url: '/dashboard/base/switches',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tables',
        url: '/dashboard/base/tables',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tabs',
        url: '/dashboard/base/tabs',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tooltips',
        url: '/dashboard/base/tooltips',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Buttons',
    url: '/dashboard/buttons',
    icon: 'icon-cursor',
    children: [
      {
        name: 'Buttons',
        url: '/dashboard/buttons/buttons',
        icon: 'icon-cursor'
      },
      {
        name: 'Dropdowns',
        url: '/dashboard/buttons/dropdowns',
        icon: 'icon-cursor'
      },
      {
        name: 'Brand Buttons',
        url: '/dashboard/buttons/brand-buttons',
        icon: 'icon-cursor'
      }
    ]
  },
  {
    name: 'Charts',
    url: '/dashboard/charts',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Icons',
    url: '/dashboard/icons',
    icon: 'icon-star',
    children: [
      {
        name: 'CoreUI Icons',
        url: '/dashboard/icons/coreui-icons',
        icon: 'icon-star',
        badge: {
          variant: 'success',
          text: 'NEW'
        }
      },
      {
        name: 'Flags',
        url: '/dashboard/icons/flags',
        icon: 'icon-star'
      },
      {
        name: 'Font Awesome',
        url: '/dashboard/icons/font-awesome',
        icon: 'icon-star',
        badge: {
          variant: 'secondary',
          text: '4.7'
        }
      },
      {
        name: 'Simple Line Icons',
        url: '/dashboard/icons/simple-line-icons',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Notifications',
    url: '/dashboard/notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'Alerts',
        url: '/dashboard/notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Badges',
        url: '/dashboard/notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'Modals',
        url: '/dashboard/notifications/modals',
        icon: 'icon-bell'
      }
    ]
  },
  {
    name: 'Widgets',
    url: '/dashboard/widgets',
    icon: 'icon-calculator',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Extras',
  },
  {
    name: 'Pages',
    url: '/dashboard/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/dashboard/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/dashboard/register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '/dashboard/404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '/dashboard/500',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Disabled',
    url: '/dashboard/dashboard',
    icon: 'icon-ban',
    badge: {
      variant: 'secondary',
      text: 'NEW'
    },
    attributes: { disabled: true },
  },
  {
    name: 'Download CoreUI',
    url: '/dashboardhttp://coreui.io/angular/',
    icon: 'icon-cloud-download',
    class: 'mt-auto',
    variant: 'success',
    attributes: { target: '_blank', rel: 'noopener' }
  },
  {
    name: 'Try CoreUI PRO',
    url: '/dashboardhttp://coreui.io/pro/angular/',
    icon: 'icon-layers',
    variant: 'danger',
    attributes: { target: '_blank', rel: 'noopener' }
  }
];
