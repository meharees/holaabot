import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpService } from '../../@core/http/http.service';
import { Router, RouterModule } from '@angular/router';
import { globalConstants } from '../../constants/global-constants';


@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private httpService: HttpService
  ) { }

  login(params) {
    return new Observable<any>(observer => {
      this.httpService
        .post(globalConstants.ENDPOINTS.LOGIN,
          params
        )
        .subscribe(res => {
          observer.next(res);
          observer.complete();
        });
    });
  }
  logout() {
  }
}
