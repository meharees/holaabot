import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {globalConstants} from '../../constants/global-constants';


@Injectable({
  providedIn: 'root',
})
export class CommonFunctionsService {

  constructor() {
  }

}
